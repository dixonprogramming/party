#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed May  3 13:45:45 2017

@author: sallymadden
"""
import random

brings = ['dessert','entree','salad']
invitees = ['buffy','giles','willow','fred','angel','spike','oz','anya','cordy','jenny','joyce']

count_invitees = len(invitees)
count_brings = len(brings)

bring_list_multiplier = (count_invitees//count_brings) + 1

brings = brings * bring_list_multiplier

random.shuffle(invitees)

party_plan = {}

response = ['yes','no']
response  = response * 6
random.shuffle(response)

for position, invitee in enumerate(invitees):
    party_plan[invitee] = {'bring':brings[position],
                           'response': response[position]}
    
print(party_plan)

for key,value in party_plan.items(): 
    print (key, " sent a response and it was ", value['response'], " and they are bringing a ", value['bring'])
    
    
    #if value['bring'] == 'dessert': if value['response'] == 'yes': print (key,value)


